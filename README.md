Com base na tabela de preços ao lado, faça um programa que leia o código de um item e a quantidade deste item. A seguir, calcule e mostre o valor da conta a pagar.

Código 1 - Cachorro Quente - R$4.00
Código 2 - X-Salada - R$4.50
Código 3 - X-Bacon - R$5.00
Código 4 - Torrada Simples - R$2.00
Código 5 - Refrigerante - R$1.50
