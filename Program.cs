﻿using System;

namespace ProvaDeLógica
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bem vindo a Lanchonete do Fael");
            Console.WriteLine("Digite o código do lanche: ");
            int codprod = int.Parse(Console.ReadLine());
            Console.WriteLine("Digite a quantidade de lanches: ");
            int qtdprod = int.Parse(Console.ReadLine());

            float valorprod1 = 4.00f;
            float valorprod2 = 4.50f;
            float valorprod3 = 5.00f;
            float valorprod4 = 2.00f;
            float valorprod5 = 1.50f;
            float valor;


            if (codprod == 1)
            {
                valor = qtdprod * valorprod1;
                Console.WriteLine($"Total: R${valor:N2}");
            }
            else if (codprod == 2)
            {
                valor = qtdprod * valorprod2;
                Console.WriteLine($"Total: R${valor:N2}");
            }
            else if (codprod == 3)
            {
                valor = qtdprod * valorprod3;
                Console.WriteLine($"Total: R${valor:N2}");
            }
            else if (codprod == 4)
            {
                valor = qtdprod * valorprod4;
                Console.WriteLine($"Total: R${valor:N2}");
            }
            else if (codprod == 5)
            {
                valor = qtdprod * valorprod5;
                Console.WriteLine($"Total: R${valor:N2}");
            }
            else
            {
                Console.WriteLine("Não existe o código digitado!");
            }
            Console.ReadKey();
        }
    }
}
